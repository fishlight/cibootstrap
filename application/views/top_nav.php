<section id="navbar">
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
    	<a class="brand" href="#">CIBoostrap</a>
		<ul class="nav">
		  <li<?php if( !$this->uri->segment(1) || $this->uri->segment(1) == 'Welcome' ) { echo ' class="active"'; }?>>
			<a href="welcome">Welcome</a>
		  </li>
		  <li class="dropdown<?php if( $this->uri->segment(1) == 'menu1' ) { echo ' active'; }?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu1<b class="caret"></b></a>
		  	<ul class="dropdown-menu">
		  		<li><a href="#">Item</a></li>
		  		<li><a href="#">Item</a></li>
		  		<li class="divider"></li>
		  		<li><a href="#">Item</a></li>
		  	</ul>
		  </li>
		  <li class="dropdown<?php if( $this->uri->segment(1) == 'menu2' ) { echo ' active'; }?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu2<b class="caret"></b></a>
		  	<ul class="dropdown-menu">
		  		<li><a href="#">Item</a></li>
		  		<li><a href="#">Item</a></li>
		  		<li><a href="#">Item</a></li>
		  	</ul>
		  </li>
		  <li><a href="/t#">Menu3</a></li>
		</ul>

		<ul class="nav pull-right">
		  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon icon-user icon-white"></i> Account<b class="caret"></b></a>
		  	<ul class="dropdown-menu">
		  		<li><a href="#">Settings</a></li>
		  		<li><a href="#">Log-out</a></li>
		  	</ul>
		  </li>
		</ul>
	</div>
  </div>
</div>
</section>