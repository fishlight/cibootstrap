<?php $this->load->view('page_header.php'); ?>

<div class="hero-unit">
  <h1>Welcome to CIBootstrap!</h1>
  <p>A basic setup for starting CodeIgniter development using Twitter's Bootstrap css &amp; javascript framework.</p>
  <p>
    <a class="btn btn-primary btn-large">
      This is a button
    </a>
  </p>
</div>

<div class="row">
	<div class="span4">
		<form class="form-vertical">
		  <fieldset>
			<legend>A Small Form</legend>
			<div class="control-group">
			  <label class="control-label" for="input01">Text input</label>
			  <div class="controls">
				<input type="text" class="input-xlarge" id="input01">
				<p class="help-block">Supporting help text</p>
			  </div>
			</div>
		  </fieldset>
		</form>

		<a class="btn" href="">Link</a>
		<button class="btn" type="submit">
		  Button
		</button>
		<input class="btn" type="button"
				 value="Input">
		<input class="btn" type="submit"
				 value="Submit">
	</div>
	<div class="span4">
		<h2>Look at my progress bar</h2>
		<div class="progress progress-info
			 progress-striped">
		  <div class="bar"
			   style="width: 20%;"></div>
		</div>
		<p>Collaboratively unleash business e-services rather than strategic benefits. Authoritatively incentivize customer directed innovation vis-a-vis pandemic internal or "organic" sources. Phosfluorescently leverage other's optimal action items for installed base communities.</p>
	</div>
	<div class="span4">
		<h2>I alert you!</h2>
		<div class="alert alert-block">
		  <a class="close" data-dismiss="alert">×</a>
		  <h4 class="alert-heading">Warning!</h4>
		  This is an alert message, just for you and your broken elephant.
		</div>
		<p>Collaboratively leverage other's leading-edge alignments and go forward portals. Efficiently generate enterprise-wide sources via resource maximizing human capital. Holisticly simplify top-line e-markets through progressive benefits. </p>
	</div>
</div>



	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
<?php $this->load->view('page_footer.php'); ?>